
#ifndef _SAM4S_EK2_H_
#define _SAM4S_EK2_H_

#include "sam4s.h"

#include "pio.h"
#include "pmc.h"

#include "trace.h"

/*
#define BOARD_REV_A
*/
#define BOARD_REV_B

/*----------------------------------------------------------------------------*/
/**
 *  \page sam4s_ek_opfreq "SAM4S-EK - Operating frequencies"
 *  This page lists several definition related to the board operating frequency
 *
 *  \section Definitions
 *  - \ref BOARD_FREQ_*
 *  - \ref BOARD_MCK
 */

/** Board oscillator settings */
#define BOARD_FREQ_SLCK_XTAL        (32768U)
#define BOARD_FREQ_SLCK_BYPASS      (32768U)
#define BOARD_FREQ_MAINCK_XTAL      (12000000U)
#define BOARD_FREQ_MAINCK_BYPASS    (12000000U)

/** Master clock frequency */
#define BOARD_MCK                   CHIP_FREQ_CPU_MAX

/** board main clock xtal startup time */
#define BOARD_OSC_STARTUP_US        15625

/*-----------------------------------------------------------------------*/
/**
 * \page sam4s_ek_board_info "SAM4S-EK - Board informations"
 * This page lists several definition related to the board description.
 *
 * \section Definitions
 * - \ref BOARD_NAME
 */

/** Name of the board */
#define BOARD_NAME "SAM4S-EK2"
/** Board definition */
#define sam4sek2
/** Family definition (already defined) */
#define sam4s
/** Core definition */
#define cortexm4

#endif  // _SAM4S_EK2_H_
