#include <SAM4S.h>
#include <stdio.h>

#include "../Serial.h"
#include "../LED.h"
#include "pio.h"
#include "tc.h"
#include "../sam4s_ek2/sam4s_ek2.h"


/*----------------------------------------------------------------------------
 *        Local definitions
 *----------------------------------------------------------------------------*/
#define PIN_TC0_TIOA1    {PIO_PA15, PIOA, ID_PIOA, PIO_PERIPH_B, PIO_DEFAULT}
#define PIN_TC0_TIOA2    {PIO_PA26, PIOA, ID_PIOA, PIO_INPUT, PIO_DEFAULT}

/*----------------------------------------------------------------------------
 *        Types
 *----------------------------------------------------------------------------*/
/** Describes a possible Timer configuration as waveform mode */
struct WaveformConfiguration {
    /** Internal clock signals selection. */
    uint32_t clockSelection;
    /** Waveform frequency (in Hz). */
    uint16_t frequency;
    /** Duty cycle in percent (positive)*/
    uint16_t dutyCycle;
};

/*----------------------------------------------------------------------------
 *        Local variables
 *----------------------------------------------------------------------------*/

/** PIOs for TC0 */
static const Pin pTcPins[] = {PIN_TC0_TIOA1, PIN_TC0_TIOA2};

/** TC waveform configurations */
static const struct WaveformConfiguration waveformConfigurations[] = {
    {TC_CMR_TCCLKS_TIMER_CLOCK4, 178, 30},
    {TC_CMR_TCCLKS_TIMER_CLOCK3, 375, 50},
    {TC_CMR_TCCLKS_TIMER_CLOCK3, 800, 75},
    {TC_CMR_TCCLKS_TIMER_CLOCK2, 1000, 80},
    {TC_CMR_TCCLKS_TIMER_CLOCK1, 4000, 55}
};

/** Current wave configuration*/
static uint8_t configuration = 0;

/** Number of available wave configurations */
const uint8_t numConfigurations = sizeof(waveformConfigurations)
                                        / sizeof(struct WaveformConfiguration);
/** Capture status*/
static uint32_t _dwCaptured_pulses;
static uint32_t _dwCaptured_ra ;
static uint32_t _dwCaptured_rb ;

/*----------------------------------------------------------------------------
 *        Local functions
 *----------------------------------------------------------------------------*/

/**
 * \brief Displays the user menu on the terminal.
 */
static void DisplayMenu(void)
{
    uint8_t i;
    printf("\n\rMenu :\n\r");
    printf("------\n\r");

    printf("  Output waveform property:\n\r");
    for (i = 0; i < numConfigurations; i++) {
        printf("  %d: Set Frequency = %4u Hz, Duty Cycle = %2u%%\n\r",
                   i,
                   (unsigned int)waveformConfigurations[i].frequency,
                   (unsigned int)waveformConfigurations[i].dutyCycle);
    }
    printf("  -------------------------------------------\n\r");
    printf("  c: Capture waveform from TC 0 channel 2\n\r");
    printf("  s: Stop capture and display informations what have been captured \n\r");
    printf("  h: Display menu \n\r");
    printf("------\n\r\n\r");
}


/**
 * \brief Interrupt handler for the TC0 channel 2.
 */
void TC2_IrqHandler( void )
{
    uint32_t status ;
    status = REG_TC0_SR2 ;

    if ( (status & TC_SR_LDRBS) == TC_SR_LDRBS )
    {
        _dwCaptured_pulses++ ;
        _dwCaptured_ra = REG_TC0_RA2 ;
        _dwCaptured_rb = REG_TC0_RB2 ;
    }
}

/**
 * \brief Configure clock, frequency and dutyclcle in wave mode.
 */
static void TcWaveformConfigure(void)
{
    const uint32_t divisors[5] = {2, 8, 32, 128, BOARD_MCK / 32768};
    uint32_t ra, rc;
    /*  Set channel 1 as waveform mode*/
    REG_TC0_CMR1 = waveformConfigurations[configuration].clockSelection  /* Waveform Clock Selection */
                   | TC_CMR_WAVE                                        /* Waveform mode is enabled */
                   | TC_CMR_ACPA_SET                                    /* RA Compare Effect: set */
                   | TC_CMR_ACPC_CLEAR                                  /* RC Compare Effect: clear */
                   | TC_CMR_CPCTRG;                                     /* UP mode with automatic trigger on RC Compare */
    rc = (BOARD_MCK / divisors[waveformConfigurations[configuration].clockSelection]) / waveformConfigurations[configuration].frequency;
    REG_TC0_RC1 = rc;
    ra = (100 - waveformConfigurations[configuration].dutyCycle) * rc / 100;
    REG_TC0_RA1 = ra;
}

/**
 * \brief Configure TC0 channel 1 as waveform operating mode.
 */
static void TcWaveformInitialize(void)
{
    volatile uint32_t dummy;
    /* Configure the PMC to enable the Timer Counter clock for TC0 channel 1. */
    PMC_EnablePeripheral(ID_TC1);
    /*  Disable TC clock */
    REG_TC0_CCR1 = TC_CCR_CLKDIS;
    /*  Disable interrupts */
    REG_TC0_IDR1 = 0xFFFFFFFF;
    /*  Clear status register */
    dummy = REG_TC0_SR1;
    /* Configure waveform frequency and duty cycle */
    TcWaveformConfigure();
    /* Enable TC0 channel 1 */
    REG_TC0_CCR1 =  TC_CCR_CLKEN | TC_CCR_SWTRG ;
    printf ("Start waveform: Frequency = %d Hz,Duty Cycle = %2d%%\n\r",
            waveformConfigurations[configuration].frequency,
            waveformConfigurations[configuration].dutyCycle);
}

/**
 * \brief Configure TC0 channel 2 as capture operating mode.
 */
static void TcCaptureInitialize(void)
{
    volatile uint32_t dummy;
    /* Configure the PMC to enable the Timer Counter clock TC0 channel 2.*/
    PMC_EnablePeripheral(ID_TC2);
    /*  Disable TC clock */
    REG_TC0_CCR2 = TC_CCR_CLKDIS;
    /*  Disable interrupts */
    REG_TC0_IDR2 = 0xFFFFFFFF;
    /*  Clear status register */
    dummy = REG_TC0_SR2;
    /*  Set channel 2 as capture mode */
    REG_TC0_CMR2 = (TC_CMR_TCCLKS_TIMER_CLOCK2    /* Clock Selection */
                   | TC_CMR_LDRA_RISING           /* RA Loading Selection: rising edge of TIOA */
                   | TC_CMR_LDRB_FALLING          /* RB Loading Selection: falling edge of TIOA */
                   | TC_CMR_ABETRG                /* External Trigger Selection: TIOA */
                   | TC_CMR_ETRGEDG_FALLING );    /* External Trigger Edge Selection: Falling edge */
}

/*----------------------------------------------------------------------------
 *         Global functions
 *----------------------------------------------------------------------------*/
/**
 * \brief Application entry point for tc_capture_waveform example.
 *
 * \return Unused (ANSI-C compatibility).
 */
int main(void)
{
    uint8_t ucKey ;
    uint16_t frequence, dutyCycle ;

	  SystemCoreClockUpdate();
	  LED_Init();                                /* LED Initialization            */
    SER_Init();                             
    /* Disable watchdog */
    //WDT_Disable( WDT ) ;

    /* Output example information */
    printf( "-- TC capture waveform example --\n\r") ;
    printf( "-- %s\n\r", BOARD_NAME ) ;
    printf( "-- Compiled: %s %s --\n\r", __DATE__, __TIME__ ) ;

    /* Configure PIO Pins for TC0 */
    PIO_Configure( pTcPins, PIO_LISTSIZE( pTcPins ) ) ;

    /* Configure TC0 channel 1 as waveform operating mode */
    printf("Configure TC0 channel 1 as waveform operating mode \n\r");
    TcWaveformInitialize() ;
    /* Configure TC0 channel 2 as capture operating mode */
    printf("Configure TC0 channel 2 as capture operating mode \n\r");
    TcCaptureInitialize() ;

    printf("Please Connect PA15 to PA26 on SAM3S-EK for wave capture test.\n\r");

    /* Configure TC interrupts for TC0 channel 2 only */
    NVIC_DisableIRQ( TC2_IRQn ) ;
    NVIC_ClearPendingIRQ( TC2_IRQn ) ;
    NVIC_SetPriority( TC2_IRQn, 0 ) ;
    NVIC_EnableIRQ( TC2_IRQn ) ;

    /* Display menu */
    DisplayMenu() ;

    while ( 1 )
    {
        ucKey = SER_GetChar() ;

        switch ( ucKey )
        {
            case 'h' :
                DisplayMenu() ;
            break ;

            case 's' :
                if ( _dwCaptured_pulses )
                {
                    REG_TC0_IDR2 = TC_IER_LDRBS ;
                    printf( "Captured %u pulses from TC0 channel 2, RA = %u, RB = %u \n\r",
                            (unsigned int)_dwCaptured_pulses, (unsigned int)_dwCaptured_ra, (unsigned int)_dwCaptured_rb ) ;

                    frequence = (BOARD_MCK / 8) / _dwCaptured_rb;
                    dutyCycle = (_dwCaptured_rb - _dwCaptured_ra) * 100 / _dwCaptured_rb;
                    printf( "Captured wave frequency = %d Hz, Duty cycle = %d%% \n\r", frequence, dutyCycle ) ;

                    _dwCaptured_pulses = 0 ;
                    _dwCaptured_ra = 0 ;
                    _dwCaptured_rb = 0 ;
                }
                else
                {
                    printf( "No waveform has been captured\n\r" ) ;
                }
                printf( "\n\rPress 'h' to display menu\n\r" ) ;
            break ;

            case 'c' :
                printf ("Start capture, press 's' to stop \n\r");
                REG_TC0_IER2 = TC_IER_LDRBS;
                /* Reset and enable the tiimer counter for TC0 channel 2 */
                REG_TC0_CCR2 =  TC_CCR_CLKEN | TC_CCR_SWTRG;
            break ;
            default :
                /* Set waveform configuration #n */
                if ( (ucKey >= '0') && (ucKey <= ('0' + numConfigurations - 1)) )
                {
                    if ( !_dwCaptured_pulses )
                    {
                        configuration = ucKey - '0' ;
                        TcWaveformInitialize() ;
                    }
                    else
                    {
                        printf( "In capturing ... , press 's' to stop capture first \n\r" ) ;
                    }
                }
            break ;
        }
    }
}
