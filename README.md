# README #

Examples of SAM4s CSP(chip support package).

### What is this repository for? ###

* Quick summary
In addition to 3U and 4L, [SAM3S-Pack](https://ww1.microchip.com/downloads/en/DeviceDoc/SAM3S8_Softpack_V1.2.1_for_MDK-ARM_4.22a.zip) can be used for both 3 series and 4 series.
The project template uses Keil Blinky (C:\Keil_v5\ARM\Boards\Atmel\SAM4S-EK\Blinky)
* Version
V1.2.1

### How do I get set up? ###

* Summary of set up
- IDE    
  Keil 5.18a & [legacy pack](http://www2.keil.com/mdk5/legacy)
- ICE    
  J-Link(v8)
* How to run tests
Use UART0 of SAM4S-EK2 as the debug port.

### Examples List ###

* tc capture waveform

### Who do I talk to? ###

* Repo owner or admin
Guibin li
* Other community or team contact
tdtc_hrb@163.com